from django.db import models

# Create your models here.
class Company(models.Model):
    name=models.CharField(max_length=50)
    legalName=models.CharField( max_length=50)
    cin_no=models.CharField( max_length=50,blank=True, null=True)
    fb_id=models.URLField( max_length=200,blank=True, null=True)
    twitter_id=models.URLField( max_length=200,blank=True, null=True)
    insta_id=models.URLField( max_length=200,blank=True, null=True)
    short_desc= models.CharField(max_length=300, blank=True, null=True)
    long_desc=models.TextField(blank=True, null=True)
    logo=models.ImageField(upload_to='images/config/',blank=True, null=True)
    geo_code_lat=models.DecimalField(max_digits=10, decimal_places=6,blank=True, null=True)
    geo_code_long=models.DecimalField(max_digits=10, decimal_places=6,blank=True, null=True)
    bg_url=models.URLField(max_length=200,blank=True, null=True)
    bg_upload= models.ImageField(upload_to='images/config/',blank=True, null=True) 
    use_url=models.BooleanField(blank=True, null=True)
    contact_no=models.CharField( max_length=50)
    alternate_no=models.CharField( max_length=50) 
    class Meta:
        verbose_name = ("Company")
        verbose_name_plural = "Companies"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Company_detail", kwargs={"pk": self.pk})
class Office(models.Model):
    company=models.ForeignKey(Company, on_delete=models.CASCADE)
    name=models.CharField(max_length=50)
    address=models.TextField(blank=True, null=True)
    city=models.CharField(max_length=50,blank=True, null=True)
    state=models.CharField(max_length=50,blank=True, null=True)
    country=models.CharField(max_length=50,blank=True, null=True)
    pincode=models.IntegerField(blank=True, null=True)    

    class Meta:
        verbose_name = ("Office")
        verbose_name_plural = ("Offices")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Office_detail", kwargs={"pk": self.pk})

class BankDetail(models.Model):
    company=models.ForeignKey(Company, on_delete=models.CASCADE)
    bank_name=models.CharField(max_length=50,blank=True, null=True)
    bank_address=models.TextField(blank=True, null=True)
    account_name=models.CharField(max_length=50,blank=True, null=True)
    account_number=models.CharField(max_length=50,blank=True, null=True)
    ifsc_code=models.CharField(max_length=50,blank=True, null=True)
    swift_code=models.CharField(max_length=50,blank=True, null=True)
    iban_code=models.CharField(max_length=50,blank=True, null=True)   

    class Meta:
        verbose_name = ("BankDetail")
        verbose_name_plural = ("BankDetails")

    def __str__(self):
        return self.bank_name

    def get_absolute_url(self):
        return reverse("BankDetail_detail", kwargs={"pk": self.pk})

class Spotlight(models.Model):
    name=models.CharField(max_length=50)
    short_desc= models.CharField(max_length=300, blank=True, null=True)
    bg_url=models.URLField(max_length=200,blank=True, null=True)
    bg_upload=models.ImageField(upload_to='images/config/',blank=True, null=True) 
    use_url=models.BooleanField(default=False)
    is_active=models.BooleanField(default=False)

    class Meta:
        verbose_name = ("Spotlight")
        verbose_name_plural = ("Spotlights")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Spotlight_detail", kwargs={"pk": self.pk})
class MediaMentions(models.Model):
    company=models.ForeignKey(Company, on_delete=models.CASCADE)
    name=models.CharField(max_length=50)
    short_desc= models.CharField(max_length=300, blank=True, null=True)
    bg_url=models.URLField(max_length=200,blank=True, null=True)
    bg_upload=models.ImageField(upload_to='images/config/',blank=True, null=True) 
    use_url=models.BooleanField(default=False)
    is_active=models.BooleanField(default=False)   

    class Meta:
        verbose_name = ("MediaMentions")
        verbose_name_plural = ("MediaMentionss")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("MediaMentions_detail", kwargs={"pk": self.pk})
class Gallery(models.Model):
    company=models.ForeignKey(Company, on_delete=models.CASCADE)
    name=models.CharField(max_length=50)
    short_desc= models.CharField(max_length=300, blank=True, null=True)
    bg_url=models.URLField(max_length=200,blank=True, null=True)
    bg_upload=models.ImageField(upload_to='images/config/',blank=True, null=True) 
    use_url=models.BooleanField(default=False)
    is_active=models.BooleanField(default=False)

    class Meta:
        verbose_name = ("Gallery")
        verbose_name_plural = ("Gallerys")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Gallery_detail", kwargs={"pk": self.pk})

class Document(models.Model):
    name = models.CharField(max_length=50)
    ref_no=models.CharField(max_length=50, blank=True, null=True)
    bg_upload=models.FileField(upload_to='files/config/docs/',blank=True, null=True) 

    class Meta:
        verbose_name = ("Document")
        verbose_name_plural = ("Documents")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Document_detail", kwargs={"pk": self.pk})





