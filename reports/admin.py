from django.contrib import admin
from .models import AssociateTransaction, Sale,PayoutRule,PayoutMonthly,PayoutLifetime,ScheduledPayment

admin.site.register(Sale)
admin.site.register(PayoutRule)
admin.site.register(PayoutLifetime)
admin.site.register(PayoutMonthly)
admin.site.register(ScheduledPayment)
admin.site.register(AssociateTransaction)
# Register your models here.
