from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from projects.models import Project,Property
from associate.models import Associate
from customers.models import Transaction

class Sale(models.Model):
    amount=models.IntegerField()
    property_ref=models.ForeignKey(Property,on_delete=models.DO_NOTHING,blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.DO_NOTHING,blank=True, null=True)
    status=models.BooleanField(default=False)
    createdAt= models.DateTimeField(auto_now_add=True)
    #status is to check whether it has been calculated or not
    @property
    def _get_year(self):
        return self.createdAt.strftime("%Y-%m")
    

    #year = property(_get_year) #EDIT

#use celery to generate monthly sale report summary of sale table
class MonthlySales(models.Model):
    MONTH = (             
        ('JAN', 'January'),
        ('FEB', 'February'),
        ('MAR', 'March'),
        ('APR', 'April'),
        ('MAY', 'May'),
        ('JUN', 'June'),
        ('JUL', 'July'),
        ('AUG', 'August'),
        ('SEP', 'September'),
        ('OCT', 'October'),
        ('NOV', 'November'),
        ('DEC', 'December'),        
    )
    amount=models.IntegerField()
    associate_ref=models.ForeignKey(Associate,on_delete=models.DO_NOTHING,blank=True, null=True)
    sale_month=models.IntegerField()
    sale_year=models.IntegerField()
    status=models.BooleanField(default=False)
    createdAt= models.DateTimeField(auto_now_add=True)
    last_updated= models.DateTimeField(auto_now=True)

#amount needs to calculated using payout table
#this table need to updated when customer pays
class PayoutRule(models.Model):
    sale_start_amount=models.IntegerField()
    sale_end_amount=models.IntegerField()
    payout=models.IntegerField()
    reward=models.IntegerField()
    is_monthly=models.BooleanField(default=False)

#after evrery calculations this table will be updated if associate fulfils rule. till 
class PayoutLifetime(models.Model):
    payout_rule=models.ForeignKey(PayoutRule,on_delete=models.DO_NOTHING,blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.DO_NOTHING,blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)

class PayoutMonthly(models.Model):
    payout_rule=models.ForeignKey(PayoutRule,on_delete=models.DO_NOTHING,blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.DO_NOTHING,blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)

# to show the earnings 
class ScheduledPayment(models.Model):
    amount=models.DecimalField(max_digits=19, decimal_places=2)
    associate_ref=models.ForeignKey(Associate,on_delete=models.DO_NOTHING,blank=True, null=True)
    status=models.BooleanField(default=False)
    payout_rule=models.ForeignKey(PayoutRule,on_delete=models.DO_NOTHING,blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)
    cus_tx=models.ForeignKey(Transaction,on_delete=models.DO_NOTHING,blank=True, null=True)

#for recived payment by the associate
class AssociateTransaction(models.Model):
    METHOD = (             
        ('C', 'Cash Payment'),
        ('Q', 'Cheque'),
        ('N', 'NEFT'),
        ('D', 'Bank Draft'),        
    )
    associate_ref=models.ForeignKey(Associate,on_delete=models.DO_NOTHING,blank=True, null=True)
    pay_method=models.CharField(max_length=1, choices=METHOD)
    date=models.DateField(blank=True,auto_now=False, auto_now_add=False)
    reference_no=models.CharField(blank=True,null=True, max_length=50)
    amount=models.DecimalField( max_digits=20, decimal_places=2,blank=True,null=True)
    description=models.TextField(blank=True,null=True)
 
