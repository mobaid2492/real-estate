# Generated by Django 2.2.2 on 2019-07-31 04:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('associate', '0021_merge_20190728_1735'),
        ('reports', '0003_monthlysales_associate_ref'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='asTransaction',
            new_name='AssociateTransaction',
        ),
        migrations.AddField(
            model_name='monthlysales',
            name='last_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='monthlysales',
            name='sale_year',
            field=models.IntegerField(default=2019),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payoutlifetime',
            name='payout_rule',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='reports.PayoutRule'),
        ),
        migrations.AlterField(
            model_name='payoutmonthly',
            name='payout_rule',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='reports.PayoutRule'),
        ),
        migrations.AlterField(
            model_name='scheduledpayment',
            name='payout_rule',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='reports.PayoutRule'),
        ),
    ]
