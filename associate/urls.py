from django.urls import path,include
from . import views

app_name="associate"
urlpatterns = [  
    path('', views.index,name='home'),
   # path('register', views.register, name='register'),
    path('project', views.projects,name='projects'),
    path('project/<int:id>/', views.projectView, name='project_view'),
    path('project/<int:proid>/section/<int:id>', views.sectionView, name='section_view'), 
    path('property/<int:id>/', views.propertyView,name='property_view'),  
    path('customers', views.customerListView, name='customers'),
    path('customers/<int:id>/', views.customerView, name='customer_view'),
    path('rewards', views.rewardView, name='rewards'),
    path('upline', views.uplineView, name='upline'), 
    path('downlines', views.downlines, name='downlines'),
    path('downlines/<int:id>', views.downlineView, name='downline_view'),   
    path('buisness-summary', views.buisnessSummary, name='buisness_summary'),
    path('team-buisness', views.teamBuisness, name='team_buisness'),
    path('leader-rewards', views.leaderRewards, name='leader_rewards'),
    path('my-earnings', views.myEarnings, name='my_earnings'),
    path('earnings-forcast', views.earningForcast, name='earning_forcast'),
    path('passbook', views.passbook, name='passbook'),
    path('profile', views.myProfile, name='profile'),
    path('profile/edit', views.profileEdit, name='profile_edit'), 
    path('changepwd', views.changePwd, name='changepwd'),              
    ]

    #project/id/edit
    