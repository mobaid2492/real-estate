from django import forms
from .models import Associate
class AssociateForm(forms.ModelForm):
    class Meta:
        model= Associate
        fields= ['user',
        'name',
        'upline',
        'contact_no',
        'alternate_no',
        'email',
        'address',
        'photo'
        ]
class UpdateForm(forms.ModelForm):
    class Meta:
        model= Associate
        fields= [
        'name',
        
        'contact_no',
        'alternate_no',
        'email',
        'address',
        'photo'
        ]

