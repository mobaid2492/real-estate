from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Associate(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE,blank=True,null=True)
    name= models.CharField(max_length=100)
    upline= models.ForeignKey('self', on_delete=models.CASCADE)
    country_code=models.CharField(max_length=50,blank=True,null=True)   
    contact_no=models.BigIntegerField(unique=True)
    alternate_no=models.BigIntegerField()
    email= models.EmailField(unique=True)
    address=models.CharField(max_length=100)
    photo=models.ImageField(upload_to='images/associate/',blank=True, null=True)
    status=models.BooleanField(default=False)
    createdAt= models.DateTimeField(auto_now_add=True)

# Create your models here.
