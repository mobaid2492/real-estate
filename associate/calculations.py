
def slab_lifetime(sales):
    num=sales
    pay=0
    fact=0
    if num<=5:       
        num=num-5
        fact=0.08
    elif num>5:
        num=num-5        
        fact=0.08
    if num>5 and num<=15:        
        num-=15
        fact=0.1
    elif num>15:
        num-=15
        fact=0.10
    if num>15 and num<=25 :        
        num=num-25
        fact=0.12
    elif num>25:
        num=num-25        
        fact=0.12
    return fact*100

def earning_calculate(sales):
    num=sales
    pay=0
    fact=0

    if num<=5:
        pay=pay+0.08*num
        num=num-5
    elif num>5:
        num=num-5
        if num>0:
            pay+=0.08*5
        fact=0.08
#################################
    if num>5 and num<=15:
        pay+=0.1*num
        num-=15
        fact=0.1
    elif num>15:
        num-=15
        if num>0:
            pay+=0.1*15
        fact=0.10
#################################
    if num>15 and num<=25 :
        pay+=0.12*num
        num=num-25
        fact=0.12
    elif num>25:
        num=num-25
        if num>0:
            pay+=0.12*25
        fact=0.12
#################################
    if num>0:
       pay=pay+fact*num
       num=0

def slab_monthly(sales):
    num=sales    
    fact=0
    if num<=25:      
        num=num-25 
    if num>25 and num<=35:
        num-=25
        fact=0.15
    elif num>35:
        num-=35        
        fact=0.15
    if num>35 and num<=50:
        fact=0.18
        num=num-35
    elif num>50:
        num=num-50       
        fact=0.18
    return fact*100

def monthly_earnings(sales):
    num=sales
    pay=0
    fact=0

    if num<=25:
        pay=pay+0.12*num
        num=num-25
    
#################################
    if num>25 and num<=35:
        pay+=0.15*num
        num-=25
    elif num>35:
        num-=35
        if num>0:
            pay+=0.15*35
        fact=0.15
#################################
    if num>35 and num<=50:
        pay+=0.18*num
        num=num-35
    elif num>50:
        num=num-50
        if num>0:
            pay+=0.18*50
        fact=0.18
#################################
    if num>0:
       pay=pay+fact*num
       num=0