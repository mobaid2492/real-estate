from django.shortcuts import render, redirect,reverse
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import auth,messages
from django.utils import timezone
from projects.models import Project,Section,Property,PriceRule,EMI
from projects.forms import ProjectForm,SectionForm,PropertyForm
from .models import Associate
from .forms import AssociateForm,UpdateForm
from customers.models import Customer,Transaction
from reports.models import MonthlySales,Sale,PayoutLifetime,PayoutMonthly,PayoutRule
from django.db.models import Sum,Count,Max
from user.views import passwordChange
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from .tasks import generate_monthly_sales_report,update_lifetime_payout
#side menu options
url="/associate/"


menu=[
    {"id":"1",
    "name":"Dashboard",
    "url": url+"",
    "icon":"fa-tachometer-alt",
    "active":"",
    "sub":[
      
        ], 
    },
    {"id":"2",
    "name":"Projects",
    "url": url+"project",
    "icon":"fa-landmark",
    "active":"",
    "sub":[
        
       
        ],
    },
    {"id":"3",
    "name":"My Business",
    "url": url+"project",
    "icon":"fa-briefcase",
    "active":"",
    "sub":[
        {"name": "View my Clients",
        "url": url+"customers", },
         {"name": "My Buisness Summary",
        "url": url+"buisness-summary", },
        {"name": "Team Buisness",
        "url": url+"team-buisness", },
        {"name": "Leader Rewards",
        "url": url+"leader-rewards", },        
         {"name": "My Buisness Rewards",
        "url": url+"rewards", },
       
        ],
    },
       {"id":"4",
    "name":"My team",
    "url": url+"project",
    "icon":"fa-users",
    "active":"",
    "sub":[
        
         {"name": "My Uplines",
        "url": url+"upline", },
        
         {"name": "My Downlines",
        "url": url+"downlines", },
        {"name": "Add New Downline",
        "url": url+"downlines", },
       
        ],
    },
       {"id":"5",
    "name":"Finance",
    "url": url+"project",
    "icon":"fa-rupee-sign",
    "active":"",
    "sub":[
        
         {"name": "My Earnings",
        "url": url+"my-earnings", },
           {"name": "Earning Forcast",
        "url": url+"earnings-forcast", },
        {"name": "Passbook",
        "url": url+"passbook", },
       
        ],
    },
    ]
#assoicate_id=user
def sidebarActive(id):
    for menuitem in menu:
        if menuitem['id']==id:
            menuitem['active']="active"
        else:
            menuitem['active']=""  
def register(request):
       form=AssociateForm(request.POST,request.FILES or None)
       if form.is_valid():
              form.save()
              messages.success(request, 'You have successfully registered!')
              return redirect(reverse('associate:home'))
       error=False
       try:     
              associate=Associate.objects.get(user=current_user)
              error=True
       except:
              error=True
       context={
              "form":form,
              "error":error,
       }
       return render(request ,'associate/register.html',context)    
 
def index(request):
        sidebarActive("1")
        current_user = request.user
        try:     
             associate=Associate.objects.values('name','photo').get(user=current_user)
        except:
              return redirect('user:login') 
        context= {
            "menu": menu,
            "associate":associate,
            "current_user":current_user,       
        }       
        return render(request ,'associate/index.html',context) 
        #39xxrqd2yi
def projects(request):
       sidebarActive("2")      
       current_user = request.user     
       try:     
              associate=Associate.objects.values('name','photo').get(user=current_user)
       except:
              return redirect('associate:register')
       project_list= Project.objects.only('id','cover_image', 'project_name', 'locality','city')
       form=ProjectForm(request.POST or None)
       if form.is_valid():
              form.save()
              form=ProjectForm(request.POST or None)

       context={
              "projects": project_list,
              "form":form,
              "menu": menu,
              "associate":associate,
       }       
       return render(request ,'associate/projects.html',context) 
##############################  PROJECTS  ######################################################
def projectView(request,id):
       sidebarActive("2")
       current_user = request.user     
       try:     
              associate=Associate.objects.values('name','photo').get(user=current_user)
       except:
              return redirect('associate:register')
       project=Project.objects.values('id',
       'project_name',
       'cover_image',
       'pro_category',
       'locality',
       'project_address',
       'city',
       'pin_code',
       'description',
       'is_active',).get(id=id)
       sections=Section.objects.all().filter(project_ref=id)
       properties=Property.objects.select_related('price_rule').all().order_by('serial_no')
       form=SectionForm(request.POST or None)
       if form.is_valid():
              form.save()
              form=SectionForm(request.POST or None)

       context={
              "project":project,
              "menu": menu,
              "associate":associate,
              "form":form,
              "sections":sections,
              "properties":properties,
       }       
       return render(request ,'associate/projectView.html',context)


##################################### SECTIONS #####################################################
def sectionView(request,proid,id):
       sidebarActive("2")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       project=Project.objects.get(id=proid)
       sections=Section.objects.get(id=id)
       properties=Property.objects.select_related('price_rule').filter(section_id=id).order_by('serial_no')
       price_rules=PriceRule.objects.all()
       form=PropertyForm(request.POST or None)
       if form.is_valid():
              form.save()
              form=PropertyForm(request.POST or None)
       context={
              "project":project,
              "menu": menu,
              "associate":associate,
              "form":form,
              "section":sections,
              "properties":properties,
              "price_rules":price_rules,
       }       
       return render(request ,'associate/sectionView.html',context) 
      

       
############################## PROPERTY #######################################################
def propertyView(request,id):
       sidebarActive("2")     
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')  
       properties=Property.objects.select_related('price_rule','section_id').get(id=id)
       paid_emi=EMI.objects.all().filter(property_ref=id).filter(status=True).aggregate(Count('emi'))
       pending_emi=EMI.objects.all().filter(property_ref=id).filter(status=False).aggregate(Count('emi'))    
       context={
              "property":properties,
              "menu": menu,
              "associate":associate,  
              "paid_emi":paid_emi,
              "pending_emi":pending_emi,                
       }       
       return render(request ,'associate/propertyView.html',context)
       
############################## CUSTOMERS #######################################################
def customerListView(request):
       sidebarActive("3")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')       
       customers=Customer.objects.all()
       properties=Property.objects.all() 
       context={
              "customers":customers,
              "menu": menu,
              "associate":associate,
              "properties":properties
                              
       }       
       return render(request ,'associate/customers.html',context)
def customerView(request,id):
       sidebarActive("3")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')       
       customer=Customer.objects.get(id=id)
       properties=Property.objects.all().filter(cust_id=id)
       emi=EMI.objects.all().filter(property_ref__cust_id=id)
       amount=Transaction.objects.all().filter(customer_ref=id).aggregate(Sum('amount'))
       tx=Transaction.objects.all().filter(customer_ref=id)
       paid_emi=EMI.objects.all().filter(property_ref__cust_id=id).filter(status=True).aggregate(Count('emi'))
       pending_emi=EMI.objects.all().filter(property_ref__cust_id=id).filter(status=False).aggregate(Count('emi'))
       context={
              "customer":customer,
              "menu": menu,                  
             "associate":associate,
             "properties":properties,
             "amount":amount,
             "emi":emi,
             "pending_emi":pending_emi,
             "paid_emi":paid_emi,
             "tx":tx,
              
       }
       return render(request ,'associate/customerView.html',context)

def rewardView(request):
       sidebarActive("3")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       context={
              
              "menu": menu,                  
             "associate":associate,
       }
       return render(request ,'associate/rewardView.html',context)

def uplineView(request):
       sidebarActive("4")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       context={
              
              "menu": menu,                  
             "associate":associate,
       }
       return render(request ,'associate/uplineView.html',context)

def downlines(request):
       sidebarActive("4")
       current_user = request.user 
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       associate_list=Associate.objects.all().filter(upline=associate)
       customers=Customer.objects.all()
       context={
              
              "menu": menu,                  
             "associate":associate,
             "associate_list":associate_list,
             "customers":customers,
       }
       return render(request ,'associate/downlines.html',context)

def downlineView(request,id):
       sidebarActive("4")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       downline=Associate.objects.get(id=id)
       downline_count=Associate.objects.all().filter(upline=id).aggregate(Count('id'))
       customers_list=Customer.objects.all().filter(associate_ref=id)
       properties= Property.objects.all()
       context={
              
              "menu": menu,                  
             "associate":associate,
             "downline":downline,
             "downline_count":downline_count,
             "customers_list":customers_list,
             "properties":properties,
       }
       return render(request ,'associate/downlineView.html',context)       

################################# Buisness #################################################
def buisnessSummary(request):
       sidebarActive("3")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       monthly_report=MonthlySales.objects.values('sale_month','amount','sale_year').filter(associate_ref=associate)
       lifetime_sales= Sale.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
       team_monthly_report=MonthlySales.objects.values('sale_month','amount').filter(associate_ref__upline=associate,createdAt__year=2019)
       payout_self=PayoutLifetime.objects.only('payout_rule').filter(associate_ref=associate).order_by('payout_rule')[:1]
       payout_self_monthly=PayoutMonthly.objects.only('payout_rule').filter(associate_ref=associate).order_by('payout_rule')[:1]
       context={
              
              "menu": menu,
              "associate":associate,
              "monthly_report":monthly_report,
              "team_monthly_report":team_monthly_report,
              "lifetime_sales":lifetime_sales,
              "payout_self":payout_self,
              "payout_self_monthly":payout_self_monthly

       }
       return render(request, 'associate/buisnessSummary.html',context)
def teamBuisness(request):
       sidebarActive("3")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       lifetime_sales= MonthlySales.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
       team_monthly_report=MonthlySales.objects.filter(associate_ref__upline=associate)
       downlines=Associate.objects.values('id','name').filter(upline=associate)
       payout_lifetime= PayoutLifetime.objects.select_related('payout_rule').only('associate_ref','payout_rule').filter(associate_ref__upline=associate)
       payout_self=PayoutLifetime.objects.filter(associate_ref=associate).aggregate(Max('payout_rule'))
       payout_monthly= PayoutMonthly.objects.select_related('payout_rule').values('associate_ref','payout_rule','createdAt').filter(associate_ref__upline=associate)
       
       context={
              
              "menu": menu,
              "associate":associate,
              "downlines":downlines,
              "team_monthly_report":team_monthly_report,
              "payout_monthly":payout_monthly,
              "payout_lifetime":payout_lifetime,
              'lifetime_sales':lifetime_sales,
              'payout_self':payout_self


       }
       return render(request, 'associate/teambuisness.html',context)
def leaderRewards(request):
       sidebarActive("3")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       context={              
              "menu": menu,
              "associate":associate,                  
       }
       return render(request, 'associate/leaderRewards.html',context)
############################## Earnings   #####################      
def myEarnings(request):
       sidebarActive("5")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       #generate_monthly_sales_report()
       #month=update_lifetime_payout(associate)
       context={
              
              "menu": menu,
              "associate":associate,
              "year":"month"                  
       }
       return render(request, 'associate/myEarnings.html',context)

def earningForcast(request):
       sidebarActive("5")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       context={
              
              "menu": menu,
              "associate":associate,                  
       }
       return render(request, 'associate/earningsForcast.html',context)
def passbook(request):
       sidebarActive("5")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       context={
              
              "menu": menu,
              "associate":associate,                  
       }
       return render(request, 'associate/passbook.html',context)

##################### Profile ###############################################
def myProfile(request):
       sidebarActive("0")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:register')
       downline_count=Associate.objects.all().filter(upline=associate).aggregate(Count('id'))
       context={
              
              "menu": menu,
              "associate":associate,
              "downline_count":downline_count                  
       }
       return render(request, 'associate/profile.html',context)
def profileEdit(request):
       sidebarActive("0")
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:profile_edit')
      
       form=UpdateForm(request.POST,request.FILES,instance=associate)
       if form.is_valid():
              form.save()
              messages.success(request, 'Your profile was updated successfully updated!')
              return redirect(reverse('associate:profile'))
       context={
              
              "menu": menu,
              "associate":associate,
              "form":form,
                       
       }
       return render(request, 'associate/profileEdit.html',context)
def changePwd(request):
       current_user = request.user     
       try:     
              associate=Associate.objects.get(user=current_user)
       except:
              return redirect('associate:changepwd')
       if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('associate:home')
        else:
            messages.error(request, 'Passwords do not match')
       else:
        form = PasswordChangeForm(request.user)
       context={              
              "menu": menu,
              "associate":associate,
              "form":form,                       
       }
       return render(request, 'user/changePassword.html',context)
       