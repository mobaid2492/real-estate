import string
from django.db.models import Sum,Count,Max
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from celery import shared_task
from reports.models import Sale,MonthlySales,PayoutRule,PayoutLifetime,PayoutMonthly
from associate.models import Associate
from .calculations import slab_lifetime,slab_monthly


@shared_task
def create_random_user_accounts(total):
    for i in range(total):
        username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        password = get_random_string(50)
        User.objects.create_user(username=username, email=email, password=password)
    return '{} random users created with success!'.format(total)


def generate_monthly_sales_report():
    sales_report=Sale.objects.only('associate_ref','createdAt','amount').order_by('createdAt').all()
    associate_list=Associate.objects.all()
    year=0
    month=0
    amount=0
    for item2 in associate_list:      
            for item in sales_report:
                if(item.createdAt.year==year) and (item.createdAt.month==month) and (item.associate_ref_id==item2.id):
                    amount+=item.amount
                else:
                    if amount>0 and item.createdAt.month>month:
                        monthly_sale=MonthlySales(amount=amount,associate_ref_id=item2.id,sale_month=month,sale_year=year)
                        monthly_sale.save()
                        amount=0                    
                    if item.createdAt.year>year:
                        year=item.createdAt.year
                        month=1
                    if item.createdAt.month>month:
                        month=item.createdAt.month
       
            year=0
            month=0
            amount=0
    return month
#send data only when lifetime sale is above 45 lakhs
def update_lifetime_payout(associate):
    lifetime_sales= Sale.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
    cal_payout=slab_lifetime(lifetime_sales['amount__sum'])
    payout_self=PayoutLifetime.objects.filter(associate_ref=associate).aggregate(Max('payout_rule'))
    if payout_self['payout_rule__max'] is not None:        
        get_payout_rule=PayoutRule.objects.get(id=payout_self['payout_rule__max'])
        if(get_payout_rule.payout>cal_payout):
           payout_update=PayoutLifetime(payout_rule=get_payout_rule,associate_ref=associate)
           payout_update.save()
    else:
        get_payout_rule=PayoutRule.objects.get(payout=cal_payout)
        payout_update=PayoutLifetime(payout_rule=get_payout_rule,associate_ref=associate)
        payout_update.save()    
    
def update_monthly_payout(associate):
    lifetime_sales= Sale.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
    #sale this month
    cal_payout=slab_lifetime(lifetime_sales['amount__sum'])
    payout_self=PayoutLifetime.objects.filter(associate_ref=associate).aggregate(Max('payout_rule'))
    if payout_self['payout_rule__max'] is not None:        
        get_payout_rule=PayoutRule.objects.get(id=payout_self['payout_rule__max'])
        if(get_payout_rule.payout>cal_payout):
           payout_update=PayoutLifetime(payout_rule=get_payout_rule,associate_ref=associate)
           payout_update.save()
    else:
        get_payout_rule=PayoutRule.objects.get(payout=cal_payout)
        payout_update=PayoutLifetime(payout_rule=get_payout_rule,associate_ref=associate)
        payout_update.save()
def team_earnings(parameter_list):
    pass
def calculate_earnings(parameter_list):
    pass
    