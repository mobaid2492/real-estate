from django import forms
from .models import Project,Section,Property,Booking,TempBooking
class ProjectForm(forms.ModelForm):
    class Meta:
        model= Project
        fields= '__all__'
class SectionForm(forms.ModelForm):
    class Meta:
        model= Section
        fields= '__all__'

class PropertyForm(forms.ModelForm):
    class Meta:
        model= Property
        fields= '__all__'

class BookingForm(forms.ModelForm):
    class Meta:
        model= TempBooking
        fields= ['property_ref',
                'customer_ref',
                'booking_date',
                'booking_revoke_date',
                ]

