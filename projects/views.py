from django.shortcuts import render
from .models import Project,Section,Property,PriceRule
from config.models import Company,Spotlight,Office,BankDetail
#from .forms import CustomerForm
from .forms import BookingForm
from customers.models import Customer

company_details=Company.objects.all()
def project(request): 
  queryset= Project.objects.all()
  r_query= queryset.filter(pro_category='R')
  c_query= queryset.filter(pro_category='C')
  context={
           "details":company_details,
           "project_list":queryset,
           "Residential_list":r_query,
           "Commercial_list":c_query,     
  }
  return render(request ,'projects/home.html',context) 

def project_profile(request,id):
  queryset= Project.objects.get(id=id)
  section_set= Section.objects.filter(project_ref=id)
  property_set= Property.objects.select_related('price_rule').all().order_by('serial_no')
  pricerules=PriceRule.objects.all()
  context={
           "details":company_details,
           "project":queryset,
           "section_set":section_set,
           "property_set":property_set,
           "pricerules":pricerules,
                
  }
  return render(request ,'projects/project-profile.html',context) 


def availability(request,id):
  project_set= Project.objects.all()
  try:
    project=Project.objects.get(id=id)
  except expression as identifier:
    project=Project.objects.all()[:1].get()
  
  section_set= Section.objects.filter(project_ref=id)
  property_set= Property.objects.select_related('price_rule').all().order_by('serial_no')
  context={
           "details":company_details,
           "project_list":project_set,
           "section_set":section_set,
           "property_set":property_set,
           "project":project
               
  }
  return render(request ,'projects/availability.html',context)  

def booking(request,id):
  properties=Property.objects.get(id=id)
  form=BookingForm(request.POST or None)
  if form.is_valid():
    form.save()
    form=BookingForm(request.POST or None)
    properties.status_code="H"
    customer=Customer.objects.get(id=request.POST["customer_ref"])
    properties.cust_id=customer
    properties.save()
               
  context={
           "details":company_details,
           "property":properties,
           "form":form,             
  }
  return render(request ,'projects/booking.html',context)