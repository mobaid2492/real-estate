from django.contrib.auth.models import User, Group
from rest_framework import serializers
from projects.models import Property,PriceRule,Section



class PriceRuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PriceRule
        fields = ('name', 'premium_price')
class SectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Section
        fields = ('id','name')
class PropertySerializer(serializers.HyperlinkedModelSerializer):
    price_rule=PriceRuleSerializer(read_only=True)
   # section_id=SectionSerializer(read_only=True)
    class Meta:
        model = Property
        fields= ('id','serial_no','status_code','price_rule')
