from django.contrib import admin
from .models import Project,Section,Property,PriceRule,Booking,TempBooking

# Register your models here.
admin.site.register(Project)
admin.site.register(Section)
admin.site.register(Property)
admin.site.register(PriceRule)
admin.site.register(Booking)
admin.site.register(TempBooking)

