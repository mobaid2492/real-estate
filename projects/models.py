from django.db import models
from django.utils import timezone
from datetime import datetime, timedelta
from customers.models import Customer

class Project(models.Model):
    CATEGORY = (
             
        ('R', 'Residential'),
        ('C', 'Commercial'),
        
    )
    project_name= models.CharField(max_length=100)
    #project_type=models.ForeignKey(Projecttype, on_delete=models.DO_NOTHING)
    pro_category=models.CharField(max_length=1, choices=CATEGORY)
    project_address= models.TextField(blank=True, null=True)
    pin_code=models.IntegerField(blank=True, null=True)
    description= models.TextField(blank=True, null=True)
    city=models.CharField(max_length=100,blank=True, null=True)
    locality=models.CharField(max_length=100)
    is_active=models.BooleanField(blank=True, null=True)
    geo_code_lat=models.DecimalField(max_digits=10, decimal_places=6,blank=True, null=True)
    geo_code_long=models.DecimalField(max_digits=10, decimal_places=6,blank=True, null=True)
    contact_person_name=models.CharField(max_length=100, blank=True, null=True)
    contact_person_phone=models.BigIntegerField(blank=True, null=True)   
    contact_person_mail=models.EmailField(blank=True, null=True)
    cover_image=models.ImageField(upload_to='images/project/',blank=True, null=True)
    layout= models.FileField(upload_to='files/project/',blank=True, null=True)
    #tags=models.TextField(blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)
 
    #deactivatedAt= models.DateTimeField(default=timezone.now())   

class Section(models.Model):
    #REduntant possible future use
    TYPE = (             
        ('L', 'Land'),
        ('A', 'Apartment'),
        ('V', 'Villa'),
        ('S', 'Shop'),
        
    )
    name=models.CharField(max_length=50)
    project_ref= models.ForeignKey(Project, on_delete=models.CASCADE)
    section_prefix=models.CharField(max_length=4)
    dimensions=models.CharField(max_length=50)
    area=models.IntegerField()
    cost_per_unit=models.IntegerField()

    class Meta:
        verbose_name = "Section"
        verbose_name_plural = "Sections"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Section_detail", kwargs={"pk": self.pk})

class PriceRule(models.Model):
    name=models.CharField(max_length=50)
    premium_price=models.IntegerField()   

class Property(models.Model):
    STATUS = (             
        ('V', 'Vacant'),
        ('H', 'Hold'),
        ('B', 'Booked'),
        ('A', 'Alloted'),
        ('R', 'Registered'),        
    )
    serial_no= models.IntegerField()
    status_code=models.CharField(max_length=1, choices=STATUS)
    cust_id=models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True, null=True)
    section_id= models.ForeignKey(Section, on_delete=models.CASCADE)
    price_rule=models.ForeignKey(PriceRule, on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Property"
        verbose_name_plural = "Properties"

    

    def get_absolute_url(self):
        return reverse("Property_detail", kwargs={"pk": self.pk})

class TempBooking(models.Model):
    property_ref=models.ForeignKey(Property, on_delete=models.CASCADE)
    customer_ref=models.ForeignKey(Customer, on_delete=models.CASCADE)
    booking_date=models.DateTimeField()
    booking_revoke_date=models.DateTimeField()
    status= models.BooleanField(default=True)
    
    class Meta:
        verbose_name = ("tempBooking")
        verbose_name_plural = ("tempBookings")


    def get_absolute_url(self):
        return reverse("tempBooking_detail", kwargs={"pk": self.pk})
class Booking(models.Model):
    property_ref=models.ForeignKey(Property, on_delete=models.CASCADE)
    customer_ref=models.ForeignKey(Customer, on_delete=models.CASCADE)
    booking_date=models.DateTimeField(auto_now_add=True)
    booking_revoke_date=models.DateTimeField(auto_now_add=True)
    status= models.BooleanField(default=True)
    
    class Meta:
        verbose_name = ("Booking")
        verbose_name_plural = ("Bookings")


    def get_absolute_url(self):
        return reverse("Booking_detail", kwargs={"pk": self.pk})
class EMI(models.Model):

    property_ref=models.ForeignKey(Property, on_delete=models.CASCADE)
    pay_day=models.DateField(blank=True,auto_now=False, auto_now_add=False)
    opening_balance=models.DecimalField(blank=True,null=True, max_digits=20, decimal_places=2)
    emi=models.DecimalField(blank=True,null=True, max_digits=20, decimal_places=2)
    intrest_paid=models.DecimalField(blank=True,null=True, max_digits=20, decimal_places=2)
    closing_balance=models.DecimalField(blank=True,null=True, max_digits=20, decimal_places=2)
    status= models.BooleanField(default=False)

    class Meta:
        verbose_name = ("emi")
        verbose_name_plural = ("emis") 

    def get_absolute_url(self):
        return reverse("emi_detail", kwargs={"pk": self.pk})
######################################
#Depriciated
#########################################
class NearByPlace(models.Model):
    place_name=models.CharField(max_length=50)
    place_icon=models.CharField(max_length=50)    
    members = models.ManyToManyField(Project, through='Project_nearPlace')
class Project_nearPlace(models.Model):
  #  date_joined = models.DateField()  
    projectref = models.ForeignKey(Project, on_delete=models.CASCADE)
    places = models.ForeignKey(NearByPlace, on_delete=models.CASCADE)
    dist= models.CharField(max_length=50)
    is_allowed=models.BooleanField() 
class Amenity(models.Model):
    amenity_name=models.CharField(max_length=50)
    amenity_icon=models.CharField(max_length=50)    
    members = models.ManyToManyField(Project, through='Project_amenity')
class Project_amenity(models.Model):
  #  date_joined = models.DateField()  
    projectref = models.ForeignKey(Project, on_delete=models.CASCADE)
    amenities = models.ForeignKey(Amenity, on_delete=models.CASCADE)
    is_allowed=models.BooleanField()         
##########################################
class Images(models.Model):
    #imageID
    #entityID
    proref = models.ForeignKey(Project, on_delete=models.CASCADE)
    image_path= models.URLField()
    is_video = models.BooleanField(default=False)
    full_video_path= models.URLField(default='')
    class Meta:
        verbose_name = "Project Image"
        verbose_name_plural = "Project Images"


