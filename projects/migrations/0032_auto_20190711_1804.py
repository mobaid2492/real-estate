# Generated by Django 2.2.2 on 2019-07-11 12:34

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0031_merge_20190711_1515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 11, 12, 33, 7, 112875, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 11, 12, 33, 7, 112875, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='project',
            name='createdAt',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 11, 12, 33, 7, 109883, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tempbooking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 11, 12, 33, 7, 111877, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tempbooking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 11, 12, 33, 7, 111877, tzinfo=utc)),
        ),
    ]
