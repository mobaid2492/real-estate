# Generated by Django 2.2.2 on 2019-07-28 11:00

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0041_auto_20190720_2026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 28, 16, 30, 16, 567288)),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 28, 16, 30, 16, 567288)),
        ),
        migrations.AlterField(
            model_name='project',
            name='createdAt',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 28, 11, 0, 16, 565290, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tempbooking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 28, 16, 30, 16, 566288)),
        ),
        migrations.AlterField(
            model_name='tempbooking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 29, 16, 30, 16, 567288)),
        ),
    ]
