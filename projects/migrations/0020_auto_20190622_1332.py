# Generated by Django 2.2.1 on 2019-06-22 08:02

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0019_auto_20190619_2019'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 22, 8, 2, 56, 440453, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 22, 8, 2, 56, 440453, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='project',
            name='createdAt',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 22, 8, 2, 56, 440453, tzinfo=utc)),
        ),
    ]
