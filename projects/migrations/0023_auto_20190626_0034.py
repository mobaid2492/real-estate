# Generated by Django 2.2.1 on 2019-06-25 19:04

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0022_auto_20190624_0150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 25, 19, 4, 46, 481914, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 25, 19, 4, 46, 481914, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='project',
            name='createdAt',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 25, 19, 4, 46, 478912, tzinfo=utc)),
        ),
    ]
