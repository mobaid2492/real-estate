# Generated by Django 2.2.1 on 2019-07-02 20:54

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0029_auto_20190630_0056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 2, 20, 54, 19, 895295, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 2, 20, 54, 19, 895295, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='project',
            name='createdAt',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 2, 20, 54, 19, 891292, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tempbooking',
            name='booking_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 2, 20, 54, 19, 894294, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tempbooking',
            name='booking_revoke_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 2, 20, 54, 19, 894294, tzinfo=utc)),
        ),
    ]
