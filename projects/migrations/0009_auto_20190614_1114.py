# Generated by Django 2.2.1 on 2019-06-14 05:44

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0008_auto_20190612_2359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='createdAt',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 14, 5, 44, 45, 692335, tzinfo=utc)),
        ),
    ]
