from django import forms
from .models import Customer
class CustomerForm(forms.ModelForm):
    class Meta:
        model= Customer
        fields= [
        'name',
        'associate_ref',
        'contact_no',
        'country_code',
        'alternate_no',
        'email',
        'address'
        ]
