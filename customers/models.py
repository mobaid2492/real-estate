from django.db import models
from associate.models import Associate
from django.utils import timezone

#from projects.models import Property

# Create your models here.

class Customer(models.Model):
    associate_ref= models.ForeignKey(Associate, on_delete=None,blank=True,null=True)
    name= models.CharField(max_length=100)
    country_code=models.CharField(max_length=50,blank=True,null=True)   
    contact_no=models.BigIntegerField(unique=True)
    alternate_no=models.BigIntegerField()
    email= models.EmailField(unique=True)
    address=models.CharField(max_length=100)
    createdAt= models.DateTimeField(auto_now_add=True)
    #date_created     

    class Meta:
        verbose_name = ("Customer")
        verbose_name_plural = ("Customers")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Customer_detail", kwargs={"pk": self.pk})


class Transaction(models.Model):
    METHOD = (
             
        ('C', 'Cash Payment'),
        ('Q', 'Cheque'),
        ('N', 'NEFT'),
        ('D', 'Bank Draft'),
        
    )
    customer_ref= models.ForeignKey(Customer, on_delete=models.CASCADE)
    pay_method=models.CharField(max_length=1, choices=METHOD)
   #propery_ref=models.ForeignKey(Property, on_delete=None,blank=True,null=True)
    date=models.DateField(blank=True,auto_now=False, auto_now_add=True)
    reference_no=models.CharField(blank=True,null=True, max_length=50)
    amount=models.DecimalField( max_digits=20, decimal_places=2,blank=True,null=True)
    description=models.TextField(blank=True,null=True)
    class Meta:
        verbose_name = ("Transaction")
        verbose_name_plural = ("Transactions")
    def get_absolute_url(self):
        return reverse("Transactions_detail", kwargs={"pk": self.pk})


