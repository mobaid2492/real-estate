from django.contrib import admin
from .models import Customer,Transaction
from projects.models import EMI
# Register your models here.
admin.site.register(Customer)
admin.site.register(EMI)
admin.site.register(Transaction)