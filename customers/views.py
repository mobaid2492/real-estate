from django.shortcuts import render,redirect
from customers.forms import CustomerForm
from .models import Customer,Transaction
from config.models import Company
from associate.models import Associate
from django.urls import reverse
from projects.models import Booking,TempBooking, EMI


# Create your views here.
company_details=Company.objects.all()
def login(request):
  context={
           "details":company_details,
                                
  }
  return render(request ,'customers/login.html', context)
def loader(request):
  context={
           "details":company_details,
                                
  }
  return render(request ,'customers/loader.html', context)
def home(request,id):
  customer=Customer.objects.get(id=id)

  emi=EMI.objects.all().filter(property_ref__cust_id=id)
  tx=Transaction.objects.all().filter(customer_ref=id)
  temp_bookings= TempBooking.objects.all().filter(customer_ref=id).order_by('id')
  bookings= Booking.objects.all().filter(customer_ref=id)
  form=CustomerForm(request.POST or None,instance=customer)
  if form.is_valid():
    form.save()
    form=CustomerForm(request.POST or None)
   
  context={
           "details":company_details,
           "form":form,
           "customer":customer,
           "emi":emi,
           "tx":tx, 
           "bookings":bookings,
           "temp_bookings":temp_bookings,        
               
  }
  return render(request ,'customers/home.html',context) 
  
def register(request):
  
  form=CustomerForm(request.POST or None)
  if form.is_valid():
    form.save()
    return redirect(reverse('customers:loader'))
  context={
           "details":company_details,
           "form":form          
               
  }
  return render(request ,'customers/registration.html',context)
  
  