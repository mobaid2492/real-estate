from django.urls import path, include

from . import views
from customers.api.views import CustomerRudView,CustomerView,CustomerCreateApiView
from django.conf.urls import url
from rest_framework import routers

urlpatterns = [
    path('rud/<int:id>', CustomerRudView.as_view()),     
    path('get/<int:contact_no>', CustomerView.as_view()),
    path('post/', CustomerCreateApiView.as_view()),
    
]