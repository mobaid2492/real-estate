from rest_framework import viewsets,status,generics
from rest_framework.views import APIView
from rest_framework.response import Response
from customers.api.serializers import CustomerSerializer
from customers.models import Customer

class CustomerView(APIView):
    def get(self, request,contact_no):
        queryset= Customer.objects.all().filter(contact_no=contact_no)
        serializer_context = {
            'request': request,
        }
        serializer= CustomerSerializer(instance=queryset,context=serializer_context, many=True)
        content = {
            'message': 'Hello, World!',            
        }
        return Response(serializer.data, headers={'Access-Control-Allow-Origin': '*'})

class CustomerRudView(generics.RetrieveUpdateDestroyAPIView):
  lookup_field=  'id'
  serializer_class= CustomerSerializer
  def get_queryset(self):    
    queryset = Customer.objects.all() # TODO
    return queryset
  #def get_object(self):
    #contact_no=self.kwargs.get('contact_no')
    #return Customer.objects.get(contact_no=contact_no)

class CustomerCreateApiView(generics.CreateAPIView):
    lookup_field= 'pk'
    serializer_class= CustomerSerializer    
    def get_queryset(self):        
        queryset= Customer.objects.all()
        return queryset