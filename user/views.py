from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User,Group
from django.contrib import auth,messages
from config.models import Company
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm


company_details=Company.objects.all()

def index(request):
  context={
           
           "details":company_details,                                 
    }
  return render(request ,'user/index.html', context)
def login(request):
    context={
           
           "details":company_details,                                 
    }
    if request.method=='POST':
        user= auth.authenticate(username=request.POST['username'],password=request.POST['password'])
        if user is not None:
            auth.login(request,user)
            if  user.groups.filter(name='Associate').exists():
              return redirect('associate:home')
            elif user.groups.filter(name='Owner').exists():
              return redirect('owner:home')
            else:
              print(user.groups.name)
              return redirect('webapp:home')
           # return redirect('associate:home')
        else:
            context={
           
           "details":company_details,
           'error':' Wrong ID or password '                                 
            }
            return render(request ,'user/login.html', context) 
    else:
        return render(request ,'user/login.html', context)


def register(request):
  context={
           
           "details":company_details,                                 
    }
  if request.method=='POST':
        try:
            if request.POST['password']==request.POST['cnf-password']:
                user = User.objects.get(username=request.POST['username'])
                context1={
           
                  "details":company_details,
                  'error':'Oops! This email is already registered'                                 
                    }
                return render(request ,'user/register.html', context1) 
        except :
           
            user= User.objects.create_user(request.POST['username'],password=request.POST['password'], email=request.POST['username'])
            group = Group.objects.get(name='Associate')
            user.groups.add(group)
            auth.login(request,user)
            if  user.groups.filter(name='Associate').exists():
              return redirect('associate:home')
            else:
              print(user.groups.name)
              return redirect('webapp:home')
  else:
        return render(request ,'user/register.html', context)   
  return render(request ,'user/register.html', context)
  
def forgotPassword(request):
  context={
           
           "details":company_details,                                 
    }
  return render(request ,'user/index.html', context)
def logout(request):
    if request.method=='POST':
        auth.logout(request)
        return redirect('webapp:home')

def passwordChange(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('user:')
        else:
            messages.error(request, 'Passwords do not match')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'user/changePassword.html', {
        'form': form
    })
    