from django.urls import path,include
from . import views

app_name="webapp"
urlpatterns = [   
  
        
    path('careers', views.careers, name='careers'),
    path('contact-us', views.contactUs, name='contact'),
    path('about-us', views.aboutUs, name='about'),  
    #path('test', views.test),
    path('', views.index,name='home'), 
    
]