from django.urls import path,include
from . import views

app_name="owner"
urlpatterns = [  
    path('', views.index,name='home'),
    path('project', views.projects,name='projects'),
    path('project/<int:id>/', views.projectView, name='project_view'),
    path('project/<int:id>/edit', views.projectEdit, name='project_edit'),
    path('project/<int:id>/delete', views.projectDelete, name='project_delete'),
    path('project/<int:proid>/section/<int:id>', views.sectionView, name='section_view'),
    path('project/<int:proid>/section/<int:id>/edit', views.sectionEdit, name='section_edit'),
    path('project/<int:proid>/section/<int:id>/delete', views.sectionDelete, name='section_delete'),
    path('property/<int:id>/', views.propertyView,name='property_view'),
    path('property/<int:id>/edit', views.propertyEdit,name='property_edit'),
    path('property/<int:id>/delete', views.propertyDelete,name='property_delete'),
    path('', views.projectView, name='customer_view'),     
    ]

    #project/id/edit
    