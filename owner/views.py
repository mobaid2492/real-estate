from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import auth
from projects.models import Project,Section,Property,PriceRule
from projects.forms import ProjectForm,SectionForm,PropertyForm


#side menu options
url="/owner/"
menu=[
    {"id":"1",
    "name":"Dashboard",
    "url": url+"",
    "icon":"fa-tachometer-alt",
    "active":"",
    "sub":[
      
        ], 
    },
    {"id":"2",
    "name":"Projects",
    "url": url+"project",
    "icon":"fa-landmark",
    "active":"",
    "sub":[
        
       
        ],
    },
    {"id":"3",
    "name":"Customers",
    "url": url+"project",
    "icon":"fa-landmark",
    "active":"",
    "sub":[
        
       
        ],
    }
    ]

def sidebarActive(id):
    for menuitem in menu:
        if menuitem['id']==id:
            menuitem['active']="active"
        else:
            menuitem['active']=""  

def index(request):
        sidebarActive("1")
        context= {
            "menu": menu,       
        }       
        return render(request ,'owner/index.html',context) 
        #39xxrqd2yi
def projects(request):
       sidebarActive("2")      
       project_list= Project.objects.all()
       form=ProjectForm(request.POST or None)
       if form.is_valid():
              form.save()
              form=ProjectForm(request.POST or None)

       context={
              "projects": project_list,
              "form":form,
              "menu": menu,
       }       
       return render(request ,'owner/projects.html',context) 
##############################  PROJECTS  ######################################################
def projectView(request,id):
       sidebarActive("2")
       project=Project.objects.get(id=id)
       sections=Section.objects.all().filter(project_ref=id)
       properties=Property.objects.all()
       form=SectionForm(request.POST or None)
       if form.is_valid():
              form.save()
              form=SectionForm(request.POST or None)

       context={
              "project":project,
              "menu": menu,
              "form":form,
              "sections":sections,
              "properties":properties,
       }       
       return render(request ,'owner/projectView.html',context)

def projectEdit(request,id):
       sidebarActive("2")
       project=Project.objects.get(id=id)
       sections=Section.objects.all().filter(project_ref=id)       
       form=ProjectForm(request.POST or None, instance=project)
       if form.is_valid():
              form.save()              

       context={
              "project":project,
              "menu": menu,
              "form":form,
              "sections":sections,
              
       }       
       return render(request ,'owner/projectEdit.html',context)
def projectDelete(request,id):
       project=Project.objects.get(id=id)
       project.delete()
       return redirect('owner:projects')
##################################### SECTIONS #####################################################
def sectionView(request,proid,id):
       sidebarActive("2")
       project=Project.objects.get(id=proid)
       sections=Section.objects.get(id=id)
       properties=Property.objects.all().filter(section_id=id)
       price_rules=PriceRule.objects.all()
       form=PropertyForm(request.POST or None)
       if form.is_valid():
              form.save()
              form=PropertyForm(request.POST or None)
       context={
              "project":project,
              "menu": menu,
              "form":form,
              "section":sections,
              "properties":properties,
              "price_rules":price_rules,
       }       
       return render(request ,'owner/sectionView.html',context)  
      
def sectionEdit(request,id,proid):
       sidebarActive("2")
       section=Section.objects.get(id=id)             
       form=SectionForm(request.POST or None, instance=section)
       if form.is_valid():
              form.save()              

       context={
              "section":section,
              "menu": menu,
              "form":form
       }       
       return render(request ,'owner/sectionEdit.html',context)
def sectionDelete(request,id,proid):
       section=Section.objects.get(id=id)
       section.delete()
       return redirect('owner:projects')
       
############################## PROPERTY #######################################################
def propertyView(request,id):
       sidebarActive("2")       
       properties=Property.objects.get(id=id)    
       context={
              "property":properties,
              "menu": menu,                  
       }       
       return render(request ,'owner/propertyView.html',context)  
      
def propertyEdit(request,id):
       sidebarActive("2")
       print(request.POST)
       prop=Property.objects.get(id=id)
       #proid=Property.section_id.project_ref.id
       #prop.section_id=Section.objects.all().filter(project_ref=proid)   
       price_rules=PriceRule.objects.all()          
       form=PropertyForm(request.POST or None, instance=prop)        
       if form.is_valid():
              form.save()              

       context={
              "property":prop,
              "menu": menu,
              "form":form,
              "price_rules":price_rules,
       }       
       return render(request ,'owner/propertyEdit.html',context)
def propertyDelete(request,id):
       properties=Property.objects.get(id=id)
       properties.delete()
       return redirect('owner:projects')
       
 

     